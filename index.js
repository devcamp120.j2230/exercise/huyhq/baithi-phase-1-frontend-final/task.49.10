/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200; // GET & PUT success
const gREQUEST_STATUS_POST_SUCCESS = 201; // status 201: POST success
const gREQUEST_STATUS_DELETE_SUCCESS = 204; // status 204: DELETE Success
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

// Biến mảng toàn cục chứa danh sách tên các thuộc tính
const gORDER_COLS = [
    "stt",
    "orderCode",
    "kichCo",
    "loaiPizza",
    "idLoaiNuocUong",
    "thanhTien",
    "hoTen",
    "soDienThoai",
    "trangThai",
    "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gORDER_ID_COL = 0;
const gORDER_CODE_COL = 1;
const gORDER_SIZE_COL = 2;
const gORDER_PIZZA_TYPE_COL = 3;
const gORDER_DRINK_TYPE_COL = 4;
const gORDER_PRICE_COL = 5;
const gORDER_FULLNAME_COL = 6;
const gORDER_PHONE_COL = 7;
const gORDER_STATUS_COL = 8;
const gORDER_ACTION_COL = 9;

// đối tượng Order sẽ được tạo mới
var vObjectRequest = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
};

var gID = null;        //id đối tượng

var gPizzaSize = {      //đối tượng kích cỡ pizza
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    soLuongNuoc: "",
    thanhTien: "",
};

var gPhanTramGiamGia = 0;  //đối tượng voucher

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    //lấy danh sách order và gán đối tượng vào DataTable
    onBtnGetAllOrderClick();
    //click thêm đơn đặt hàng
    onClickBtnThemOrder();
    //click update order
    onClickBtnUpdateOrder();
    //click delete
    onClickBtnDelete();
    
    //click btn filter
    onClickBtnFilter();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham them order
function onClickBtnThemOrder() {
    $("#btn-create-new").on("click", function () {
        //show modal
        $("#modal-create").modal("show");
        //them du lieu vao select kích cỡ pizza
        getDataPizzaSizeSelect("select-pizza-size-create");
        //kiem tra kích cỡ pizza, load dữ liệu vào input
        onPizzaSizeChange();
        //them du lieu vao select loai pizza
        getDataPizzaTypeSelect("select-pizza-type-create");
        //goi Api lấy danh sách đồ uống, thêm vào select
        onBtnGetDrinkListClick("select-drink-create");
        //click confirm order
        onClickBtnThemOrderConfirm();
    })
}
//load du lieu khi kích cỡ pizza thay đổi
function onPizzaSizeChange() {
    $("#select-pizza-size-create").on("change", function () {
        var vSizePizza = $(this).val();
        //lay du lieu obj kich co pizza
        gPizzaSize = themDuLieuInputKichCo(vSizePizza)
        //them du lieu vao input
        $("#inp-duong-kinh-create").val(gPizzaSize.duongKinh);
        $("#inp-suon-create").val(gPizzaSize.suon);
        $("#inp-salad-create").val(gPizzaSize.salad);
        $("#inp-soluong-nuoc-create").val(gPizzaSize.soLuongNuoc);
        $("#inp-thanh-tien-create").val(gPizzaSize.thanhTien);
    })
}

//ham confirm them order
function onClickBtnThemOrderConfirm() {
    $("#btn-confirm-create").on("click", function () {
        //them du lieu form vao Obj
        themDuLieuObj(vObjectRequest, "create");
        //kiem tra du lieu
        if (validateObj(vObjectRequest)) {
            console.log(vObjectRequest)
            //hide modal
            $("#modal-create").modal("hide");
            alert("Thêm Order thành công!");
            //gọi Api thêm order
            onBtnCreateOrderClick(vObjectRequest)
        }
    })
}
//click button update
function onClickBtnUpdateOrder() {
    //click edit
    $("#table-users tbody").on("click", ".edit-grade", function () {
        onUserUpdateClick(this);
    });
}
//click button xác nhận đơn hàng
function onClickBtnConfirmUpdateOrder() {
    //click confirm edit
    $("#btn-confirm-edit").on("click", function () {
        //gọi Api
        onBtnUpdateOrderClick(gID, "confirm");
        //ẩn modal
        $("#modal-edit").modal("hide");
    })
}
//click button hủy đơn hàng
function onClickBtnCancelUpdateOrder() {
    //click confirm edit
    $("#btn-cancel-edit").on("click", function () {
        //gọi Api
        onBtnUpdateOrderClick(gID, "cancel");
        //ẩn modal
        $("#modal-edit").modal("hide");
    })
}

//hàm xử lý click button delete
function onClickBtnDelete() {
    //click delete
    $("#table-users tbody").on("click", ".delete-grade", function () {
        onClickDelete(this);
    });
}
//hàm xử lý click button confirm delete
function onClickBtnConfirmDelete() {
    //click confirm delete
    $("#btn-confirm-delete").on("click", function () {
        console.log(gID);
        //goi API
        onBtnDeleteOrderClick(gID);
        //xu ly fronend
        $("#delete-confirm-modal").modal("hide");
    })
}

//hàm xử lý khi click filter
function onClickBtnFilter() {
    $("#btn-filter").on("click", function () {
        onBtnGetAllOrderClick();
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm thêm data vào datatable
function getDataOnDatatable(gJsonObj) {
    // var gUserObj = JSON.parse(gJsonUser);
    $("#table-users").DataTable().destroy();
    $("#table-users").DataTable({
        data: gJsonObj,
        columns: [
            { data: gORDER_COLS[gORDER_ID_COL] },
            { data: gORDER_COLS[gORDER_CODE_COL] },
            { data: gORDER_COLS[gORDER_SIZE_COL] },
            { data: gORDER_COLS[gORDER_PIZZA_TYPE_COL] },
            { data: gORDER_COLS[gORDER_DRINK_TYPE_COL] },
            { data: gORDER_COLS[gORDER_PRICE_COL] },
            { data: gORDER_COLS[gORDER_FULLNAME_COL] },
            { data: gORDER_COLS[gORDER_PHONE_COL] },
            { data: gORDER_COLS[gORDER_STATUS_COL] },
            { data: gORDER_COLS[gORDER_ACTION_COL] },
        ],
        columnDefs: [
            {
                targets: gORDER_ID_COL,
                render: function (data, type, row, meta) {
                    return (meta.row + 1);
                }
            },
            // {
            //     targets: gORDER_STATUS_COL,
            //     render: classNameData
            // },
            {
                targets: gORDER_ACTION_COL,
                defaultContent: `
                <img class="edit-grade" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                <img class="delete-grade" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
              `
            }
        ]
    });
}

// hàm xử lý khi click vào nút user detail
function onUserUpdateClick(paramUserDetailBtn) {
    var vUserDetailBtn = $(paramUserDetailBtn);
    var gUserTable = $("#table-users").DataTable();
    var vTableRow = vUserDetailBtn.parents("tr");
    var vDataTableRow = gUserTable.row(vTableRow);
    var vData = vDataTableRow.data();
    //lấy id
    gID = vData.id;
    //lấy orderCode
    var vOrderCode = vData.orderCode;
    //goi Api lấy dữ liệu với orderCode
    onBtnGetOrderByIdClick(vOrderCode);
    //show modal
    $("#modal-edit").modal("show");
    //xử lý khi confirm
    onClickBtnConfirmUpdateOrder();
    //xử lý khi cancel
    onClickBtnCancelUpdateOrder();
}
//hàm xử lý khi xóa
function onClickDelete(paramStudent) {
    var vUserDetailBtn = $(paramStudent);
    var gUserTable = $("#table-users").DataTable();
    var vTableRow = vUserDetailBtn.parents("tr");
    var vDataTableRow = gUserTable.row(vTableRow);
    var vData = vDataTableRow.data();
    gID = vData.id;
    $("#delete-confirm-modal").modal("show");
    //click confirm delete
    onClickBtnConfirmDelete();
}
//ham them du lieu kích cỡ pizza vao select
function getDataPizzaSizeSelect(id) {
    var vObj = ["S", "M", "L"];
    var vSelectElm = $("#" + id);
    for (let i = 0; i < vObj.length; i++) {
        $("<option>", {
            value: vObj[i],
            html: vObj[i]
        }).appendTo(vSelectElm);
    }
}
//thêm dữ liệu vào input ứng với kích cỡ pizza
function themDuLieuInputKichCo(param) {
    const gSizePizzaS = {
        kichCo: "S",
        duongKinh: "20",
        suon: "2",
        salad: "200",
        soLuongNuoc: "2",
        thanhTien: "150000",
    }
    const gSizePizzaM = {
        kichCo: "M",
        duongKinh: "25",
        suon: "4",
        salad: "300",
        soLuongNuoc: "3",
        thanhTien: "200000",
    }
    const gSizePizzaL = {
        kichCo: "L",
        duongKinh: "30",
        suon: "8",
        salad: "500",
        soLuongNuoc: "4",
        thanhTien: "250000",
    }

    const gSizeNull = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        soLuongNuoc: "",
        thanhTien: "",
    }

    if (param == "S") {
        return gSizePizzaS;
    } else if (param == "M") {
        return gSizePizzaM;
    } else if (param == "L") {
        return gSizePizzaL;
    }
    return gSizeNull;
}
//ham them du lieu loại pizza vao select
function getDataPizzaTypeSelect(id) {
    var vObj = [
        { "id": "Seafood", "name": "Hải sản" },
        { "id": "Hawaii", "name": "Hawaii" },
        { "id": "Bacon", "name": "Thịt hun khói" }
    ]
    var vSelectElm = $("#" + id);
    for (let i = 0; i < vObj.length; i++) {
        $("<option>", {
            value: vObj[i].id,
            html: vObj[i].name
        }).appendTo(vSelectElm);
    }
}
//ham them du lieu đồ uống vào select
function getDataDrinkSelect(paramObj, id) {
    var vSelectElm = $("#" + id);
    for (let i = 0; i < paramObj.length; i++) {
        $("<option>", {
            value: paramObj[i].maNuocUong,
            html: paramObj[i].tenNuocUong
        }).appendTo(vSelectElm);
    }
}
//them du lieu ObjRequest
function themDuLieuObj(paramObj, id) {
    paramObj.kichCo = gPizzaSize.kichCo;
    paramObj.duongKinh = gPizzaSize.duongKinh
    paramObj.suon = gPizzaSize.suon;
    paramObj.salad = gPizzaSize.salad;
    paramObj.loaiPizza = $("#select-pizza-type-" + id).val();
    paramObj.idVourcher = $("#inp-voucher-" + id).val();
    paramObj.idLoaiNuocUong = $("#select-drink-" + id).val();
    paramObj.soLuongNuoc = gPizzaSize.soLuongNuoc
    paramObj.hoTen = $("#inp-name-" + id).val();
    paramObj.thanhTien = gPizzaSize.thanhTien;
    paramObj.email = $("#inp-email-" + id).val();
    paramObj.soDienThoai = $("#inp-phone-" + id).val();
    paramObj.diaChi = $("#inp-address-" + id).val();
    paramObj.loiNhan = $("#txtarea-message-" + id).val();
}
//thêm dữ liệu Obj vào form
function themDuLieuVaoForm(paramObj) {
    $("#inp-pizza-size-update").val(paramObj.kichCo);
    $("#inp-duong-kinh-update").val(paramObj.duongKinh);
    $("#inp-suon-update").val(paramObj.suon);
    $("#inp-salad-update").val(paramObj.salad);
    $("#inp-soluong-nuoc-update").val(paramObj.soLuongNuoc);
    $("#inp-thanh-tien-update").val(paramObj.thanhTien);
    $("#inp-pizza-type-update").val(paramObj.loaiPizza);
    $("#inp-drink-update").val(paramObj.idLoaiNuocUong);
    $("#inp-name-update").val(paramObj.hoTen);
    $("#inp-email-update").val(paramObj.email);
    $("#inp-phone-update").val(paramObj.soDienThoai);
    $("#inp-address-update").val(paramObj.diaChi);
    $("#inp-voucher-update").val(paramObj.idVourcher);
    $("#txtarea-message-update").val(paramObj.loiNhan);
}
//kiem tra du lieu ObjRequest
function validateObj(paramObj) {
    if (paramObj.kichCo == 0) {
        alert("Hãy chọn kích cỡ Pizza.");
        return false;
    }

    if (paramObj.loaiPizza == 0) {
        alert("Hãy chọn loại Pizza.");
        return false;
    }
    if (paramObj.idLoaiNuocUong == 0) {
        alert("Hãy chọn loại đồ uống.");
        return false;
    }
    if (paramObj.hoTen == "") {
        alert("Họ tên không được để trống.");
        return false;
    }
    if (!validateEmail(paramObj.email)) {
        alert("Email không đúng định dạng.");
        return false;
    }
    if (paramObj.soDienThoai == "") {
        alert("Số điện thoại không được để trống.");
        return false;
    }
    if (paramObj.diaChi == "") {
        alert("Địa chỉ không được để trống.");
        return false;
    }
    var vCheckVoucher = onBtnCheckVoucherClick(paramObj.idVourcher);
    if (!vCheckVoucher) {
        alert("Voucher không đúng.");
        return false;
    }
    return true;
}
//validate email
function validateEmail(vEmail) {
    if (vEmail != "") {
        var vRegEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        return vEmail.match(vRegEmail);
    }
    return true;
}
// //filter theo ten mon hoc
function filterStudent(vparamObj) {
    var vPizzaTypeFilter = $("#select-pizza-type-filter").val();
    var vStatusFilter = $("#select-status-filter").val();
    if (vPizzaTypeFilter != "0" || vStatusFilter != "0") {
        var vNewObj = vparamObj.filter(function (vObj) {
            if (vPizzaTypeFilter == "0") {
                return vObj.trangThai == vStatusFilter;
            } else if (vStatusFilter == "0") {
                return vObj.loaiPizza == vPizzaTypeFilter;
            } else {
                return (vObj.loaiPizza == vPizzaTypeFilter && vObj.trangThai == vStatusFilter);
            }

        })
        return vNewObj;
    }
    return vparamObj;
}
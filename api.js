

//gọi mẫu lấy danh sách order
function onBtnGetAllOrderClick() {
    "use strict";
    // create a request
    var vXmlHttpGetAllOrder = new XMLHttpRequest();
    vXmlHttpGetAllOrder.open("GET", gBASE_URL, true);
    vXmlHttpGetAllOrder.send();

    vXmlHttpGetAllOrder.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            var vOrderList = vXmlHttpGetAllOrder.responseText;
            var vOrderObj = JSON.parse(vOrderList);
            // console.log(vOrderList);
            //gán dữ liệu dataTable
            var vObj = filterStudent(vOrderObj)
            getDataOnDatatable(vObj);
        }
    }

}
//gọi mẫu lấy thông tin 1 order - dùng khi xem chi tiết để sửa đơn hàng
function onBtnGetOrderByIdClick(vOrderCode) {
    "use strict";
    //base url
    // const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    // var vOrderCode = "93zj2Vzrr6";

    var vXmlHttpGetOrderById = new XMLHttpRequest();
    vXmlHttpGetOrderById.open("GET", gBASE_URL + "/" + vOrderCode, true);
    vXmlHttpGetOrderById.send();
    vXmlHttpGetOrderById.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            // console.log(vXmlHttpGetOrderById.responseText);
            var vObj = JSON.parse(vXmlHttpGetOrderById.responseText);
            //thêm dữ liệu vào form update
            themDuLieuVaoForm(vObj);
        }
    }
}

//gọi tạo một order mới
function onBtnCreateOrderClick(vObjectRequest) {
    "use strict";
    //base url
    // const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    

    var vXmlHttpCreateOrder = new XMLHttpRequest();
    vXmlHttpCreateOrder.open("POST", gBASE_URL, true);
    vXmlHttpCreateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    vXmlHttpCreateOrder.send(JSON.stringify(vObjectRequest));
    vXmlHttpCreateOrder.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_POST_SUCCESS) {
            // var vCreatedOrder = vXmlHttpCreateOrder.responseText;
            // console.log(vCreatedOrder);
            //gọi APi load lại dữ liệu
            onBtnGetAllOrderClick()
        }
    }
}

//update một order mới
function onBtnUpdateOrderClick(vId, status) {
    "use strict";
    //base url
    // const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    // var vId = "91";
    var vObjectRequest = {
        trangThai: status //3 trang thai open, confirmed, cancel
    }

    var vXmlHttpUpdateOrder = new XMLHttpRequest();
    vXmlHttpUpdateOrder.open("PUT", gBASE_URL + "/" + vId);
    vXmlHttpUpdateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    vXmlHttpUpdateOrder.send(JSON.stringify(vObjectRequest));
    vXmlHttpUpdateOrder.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            // var vUpdatedOrder = vXmlHttpUpdateOrder.responseText;
            alert("Thay đổi trạng thái đơn hàng thành công.")
            //load lai danh sách
            onBtnGetAllOrderClick();
        }
    }
}

//delete một order
function onBtnDeleteOrderClick(vId) {
    "use strict";
    //base url
    var vXmlHttpUpdateOrder = new XMLHttpRequest();
    vXmlHttpUpdateOrder.open("DELETE", gBASE_URL + "/" + gID, true);
    vXmlHttpUpdateOrder.send();
    vXmlHttpUpdateOrder.onreadystatechange = function () {
        // Delete thành công thì thông báo kết quả cho người dùng
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_DELETE_SUCCESS) {
            alert("Delete " + vId + " success!");
            //goi Api lay danh sach order
            onBtnGetAllOrderClick();
        }
    }
}

//check mã giảm giá
function onBtnCheckVoucherClick(vVoucherId) {
    if (vVoucherId != "") {
        // var vVoucherId = "24864";  //một số mã đúng để test: 95531, 81432,...lưu ý test cả mã sai
        "use strict";
        const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
        // nếu mã giảm giấ đã nhập, tạo vXmlHttp request và gửi về server
        var vXmlHttp = new XMLHttpRequest();
        vXmlHttp.open("GET", vBASE_URL + vVoucherId, false);
        vXmlHttp.send();
        if (vXmlHttp.status == gREQUEST_STATUS_OK) { // restFullAPI successful
            // nhận lại response dạng JSON ở vXmlHttp.responseText và chuyển thành object
            console.log(vXmlHttp.responseText);
            var vVoucher = JSON.parse(vXmlHttp.responseText);
            gPhanTramGiamGia = vVoucher.phanTramGiamGia;
            return true;
        }
        else {
            // không nhận lại được data do vấn đề gì đó: khả năng mã voucher ko dúng
            console.log("Không tìm thấy voucher " + vXmlHttp.responseText);
            gPhanTramGiamGia = 0;
            return false;
        }
    }
    return true;
}

//mã nguồn để load data drink list (danh sách loại nước uống) về
function onBtnGetDrinkListClick(vSelectElm) {
    "use strict";
    var vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/drinks";
    var vXhttp = new XMLHttpRequest();
    vXhttp.open("GET", vBASE_URL, true);
    vXhttp.send();
    vXhttp.onreadystatechange = function () {
        if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
            console.log(vXhttp.responseText); //ghi response text ra console.log
            var gJsonSubject = JSON.parse(vXhttp.responseText);
            //them dữ lieu vao select drink
            getDataDrinkSelect(gJsonSubject, vSelectElm);
        }
    };
}